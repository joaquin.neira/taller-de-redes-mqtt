                               ACTIVIDAD 2 TALLER DE REDES Y SERVICIOS 
                           Integrantes: Christian Bastias y Joaquín Neira

En esta actividad, se realizó una implementación de una dupla de software asociada a un procolo de capa de aplicación.

El software que se escogió para esta actividad es HiveMQ, un sistema de comunicación entre dispositivos de IoT, sostenido por el protocolo MQTT. Este protocolo provee las herramientas para el establecimiento de redes de comunicación de telemetría.

MQTT y HiveMQ funcionan bajo un patrón de diseño de Publicador/Subscriptor, en donde un intermediario, llamado Corredor o Broker, reenvía la información proveida por un Publicador hacia todos aquelos Subscriptores de su información.

La versión de Broker utilizada es HiveMQ Community Edition, disponible en https://github.com/hivemq/hivemq-community-edition.

La versión del cliente utilizada es HiveMQ CLI  disponible en https://github.com/hivemq/mqtt-cli.

El proceso de implementación de esta aplicación puede verse en más detalle en el informe adjunto con la entrega de esta actividad.


A continuación, se adjunta el vídeo solicitado:

[Entrega Actividad 2](https://www.youtube.com/watch?v=vyaI21yKll4)


[Entrega Actividad 3](https://youtu.be/DowQ9Wbd9kk)


[Entrega Actividad 4](https://youtu.be/bhATEdvYIxY)
