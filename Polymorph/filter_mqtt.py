def filter_mqtt(packet):
    try:
        if packet["TCP"]["dstport"] == 1883:
            if packet["MQTT"]["hdrflags"] == "0x30":
                print("Topic:", packet["MQTT"]["topic"])
                print("Msg:", packet["MQTT"]["msg"])
                return packet
    except:
        return None